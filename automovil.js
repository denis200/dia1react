class Auto{
    constructor(tipo,anio,hasSoat){
    this.tipo=tipo;
    this.anio=anio;
    this.hasSoat=hasSoat;
}
getTipo(){
 return this.tipo;
}
getAnio(){
  return this.anio;
}
getHasSoat(){
    return this.hasSoat;
  }
setTipo(tipo){
    this.tipo=tipo;
}
setAnio(anio){
    this.anio=anio;
}
setHasSoat(hasSoat){
    this.hasSoat=hasSoat;
}
}
class Toyota extends Auto{
    constructor(tipo,anio,hasSoat,modelo){
        super(tipo,anio,hasSoat);
    this.modelo=modelo;
    }
    getModelo(){
        return this.modelo;
       }
       setModelo(modelo){
           this.modelo=modelo;
       }
    
}
const toyota = new Toyota('Vagoneta',2018,true,'volvo');
toyota.setTipo('trufi');
toyota.setAnio(2015);
toyota.setHasSoat(false);
toyota.setModelo('rav4');

console.log(toyota)

